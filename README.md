Dissipative and dispersive cavity optomechanics with a frequency-dependent mirror
=================================================================================

Code to calculate the optical and optomechanical properties of the cavity optomechanical setup from the article [arXiv:2311.15311](https://arxiv.org/abs/2311.15311)

> J. Monsel, A. Ciers, S. K. Manjeshwar, W. Wieczorek, J. Splettstoesser: Dissipative and dispersive cavity optomechanics with a frequency-dependent mirror. *Phys. Rev. A* **109**, 043532 (2024)

Please cite the article if you use this code.

BibTex citation:

```
@article{Monsel2024Apr,
	author = {Monsel, Juliette and Ciers, Anastasiia and Manjeshwar, Sushanth Kini and Wieczorek, Witlef and Splettstoesser, Janine},
	title = {{Dissipative and dispersive cavity optomechanics with a frequency-dependent mirror}},
	journal = {Phys. Rev. A},
	volume = {109},
	number = {4},
	pages = {043532},
	year = {2024},
	month = {apr},
	publisher = {American Physical Society},
	doi = {10.1103/PhysRevA.109.043532}
}
```

Repository content
------------------

* `optomechanics.py`: python module containing the functions to compute the quantities defined in the article.
* `figures.ipynb`: Jupyter notebook plotting those quantities for the devices defined in the article.
* `cooling_limit_fct_sideband_res.npz`, `cooling_limit_fct_sideband_res_std.npz`: data files that can be recomputed from the notebook.


Dependencies
------------

This code was written using the following packages and librairies:

* Python 3.11
* NumPy 1.26
* SciPy 1.11
* Jupyter Notebook 7.0